.PHONY: clean All

All:
	@echo "----------Building project:[ unitestmod - Release ]----------"
	@cd "unitestmod" && $(MAKE) -f  "unitestmod.mk" PreBuild && $(MAKE) -f  "unitestmod.mk"
clean:
	@echo "----------Cleaning project:[ unitestmod - Release ]----------"
	@cd "unitestmod" && $(MAKE) -f  "unitestmod.mk" clean
