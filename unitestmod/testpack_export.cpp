#include "testpack_export.h"
#include "exportclasstest.h"
#include <boost/test/unit_test.hpp>

using boost::unit_test_framework::test_suite;
using boost::unit_test_framework::test_case;
class testpack_export:public test_suite
{
public:
	testpack_export():test_suite("ExportTest") 
   {
	
	// creating instace of the class to be tested
     boost::shared_ptr<exportclasstest> instance(new exportclasstest());

      //creating test cases by passing the instance 
      test_case* testRegisterExport = BOOST_CLASS_TEST_CASE(&exportclasstest::CheckRegisteredtest, instance );
      //test_case* propertyVal = BOOST_CLASS_TEST_CASE(&TestAnonymous::PropertyValTester, instance );
      //adding test cases in test suite
	  add(testRegisterExport);
	  //add(propertyVal);

   }
   


	~testpack_export(){}
	

};
