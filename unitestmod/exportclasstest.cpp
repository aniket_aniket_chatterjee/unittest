#include "exportclasstest.h"
#include "exportclass.h"
#include "exportobject.h"
#include "anonymousjsobjectproxy.h"
#include "anonymousjsobject.h"
#include "js_console.h"

	class HelpClass
{
public:
        HelpClass(){ printf("In default constructor of HelpClass \n"); }
        HelpClass(std::string&  p) { 
                printf("In constructor of HelpClass %s\n", p.c_str() );
                s=p;
        }
        std::string s;
public:
        int sfunc() { 
                std::cout << s << "\n"; 
                return s.size();
        }
        

};

class Cc {
public:
        Cc(int pi) { 
                printf("In constructor of c %d\n",pi );
                i=pi;
                d=3.33;
                s="444";
        }

        char c;
        int i;
        double d;
        std::string s;
        HelpClass h;
public:
        int sfunc(HelpClass& a) { 
                std::cout <<  "cc::sfunc HelpClass&  \n";
                return 888;
        }
        
};
exportclasstest::exportclasstest()
{
}

exportclasstest::~exportclasstest()
{
}

void exportclasstest::CheckRegisteredtest(){
		JSEngine _js;
        int iret =  _js.Init();
        if(iret!=0)
        {
                printf("init failed\n");
        }
        defConsole(_js);
        exporterCLass<HelpClass> *pech = exporterCLass<HelpClass>::CreateInstance("testHelpClass");
        pech = exporterCLass<HelpClass>::GetInstance();
        pech->setConstructor(init<std::string>()).
        add<int,&HelpClass::sfunc>("sfunc", &HelpClass::sfunc).
        
        setVariable< std::string, &HelpClass::s >("svar");
        pech->registerEngine(_js);

        exporterCLass<Cc> *pec = exporterCLass<Cc>::CreateInstance("testclass");
        pec = exporterCLass<Cc>::GetInstance();
        pec->setConstructor(init<int >()).
        add<int, HelpClass&, &Cc::sfunc>("sfunc", &Cc::sfunc)
        .setVariable< int , &Cc::i >("ivar")
        .setVariable< double, &Cc::d >("dvar")
        .setVariable< std::string, &Cc::s >("svar");
        pec->registerEngine(_js);
        
        int eret = _js.CompileExecuteFile("/home/anandrathi/sre/libcdr/libjspp_util/TestJSScriptExportClass.js");
	}
	
	