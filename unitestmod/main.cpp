// Boost.Test    void   testPropertyVal(int i)

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/test/parameterized_test.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/output_test_stream.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/detail/unit_test_parameters.hpp>
#include <boost/test/included/unit_test_framework.hpp>

// BOOST
#include <boost/functional.hpp>
#include <boost/static_assert.hpp>
#include <boost/mem_fn.hpp>
#include <boost/bind.hpp>

// STL
#include "test_pack.cpp"
#include "testpack_export.cpp"
using namespace boost::unit_test;
using boost::unit_test_framework::test_suite;



test_suite* init_unit_test_suite(int /* argc */, char** /* argv */)
{
   // create the top test suite
   
   //boost::shared_ptr<TestPack> instance(new TestPack);GetSuite_FutureTest
     framework::master_test_suite().add(new AnonymousTestPack());  
	framework::master_test_suite().add(new testpack_export());
    return 0;
}