
#include <string>
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <list>
#include <fstream>
#include "anonymousjsobjectproxy.h"
#include "anonymousjsobject.h"
#include "sproxyFunc_inc.h"
#include "js_console.h"
#include "test_anonymous.h"
#include <boost/test/unit_test.hpp>
using namespace boost::unit_test;
using boost::unit_test_framework::test_suite;
void atestfunc()
{
	std::cout << "This is not a Drill soldier \n";
}
void testfunca(std::string a)
{
	std::cout << "testfunc a= " << a << "\n";
}
void testfuncb(std::string& a)
{
	std::cout << "testfunc a= " << a << "\n";
}
void testfuncc(int& a)
{
	std::cout << "testfunc a= " << a << "\n";
}
void testfuncd(double& a)
{
	std::cout << "testfunc a= " << a << "\n";
}


		void TestAnonymous::GenerateScript() {
				std::string scriptStr = "\n\
				CONSOLE.writeln(RECORD.CALLER_NUMBER); \n\
				CONSOLE.writeln(RECORD.CALLED_NUMBER); \n\
				CONSOLE.writeln(RECORD.shx); \n\
				RECORD.shx=\"5432 5432 5432 5432 5432 5432 5432 5432 5432 54321 53\" ; \n\
				RECORD.shx=\"5432 5432 5432 5432 5432 5432 5432 5432 48\" ; \n\
				RECORD.testfunc();\n\
				RECORD.testfunca(\" test string\"); \n\
				RECORD.testfuncb(\" test string b\"); \n\
				RECORD.testfuncc(1234); \n\
				RECORD.testfuncd(111.222); \n\
				CONSOLE.writeln('hello') ;\n";
				
			  std::ofstream myfile;
			  myfile.open ("test.js");
			  myfile << scriptStr.c_str();
			  myfile.close();        
				
			}

		//____________________________________________________________________________//
		  void TestAnonymous::PropertyValTester()
		  {
				int param[]={12,44,13,1,4};
					for(int i=0;i<5;i++)
					{
							 BOOST_WARN( test_propertyVal( param[i]));
							
					}
					
		  }
		  bool TestAnonymous:: test_propertyVal(int i) {
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
				}
				
				defConsole(_js);
				AnonymousJSObject *pdj = new AnonymousJSObject("RECORD",_js);
				std::string sval = "1234 6";
				pdj->SetPropertyVal((char*)"CALLER_NUMBER","1234",false,false);
				pdj->SetPropertyVal((char*)"CALLER_NUMBER","1234",false,false);
				pdj->SetPropertyVal((char*)"CALLED_NUMBER","4321",false,false);
				//pdj->DefPropertyValExternalString("shx",sval);
				
				pdj->SetPropertyValExternalString("shx", "1234 1234 1234 1234 1234 1234 1234 1234 1234 12345 13");
				pdj->addFunc<atestfunc>("testfunc", atestfunc);		
				pdj->addFunc<void, std::string, testfunca>("testfunca", testfunca);
				pdj->addFunc<void, std::string&, testfuncb>("testfuncb", testfuncb);
				pdj->addFunc<void, int&, testfuncc>("testfuncc", testfuncc);
				pdj->addFunc<void, double&, testfuncd>("testfuncd", testfuncd);
				int eret = _js.CompileExecuteFile("test.js");
				BOOST_CHECK(eret==0);
					if(eret!=0)
					{
						return false;
						}
						else
						{
							return true;
							}
				}
		TestAnonymous::TestAnonymous()
		{
		}
		TestAnonymous::~TestAnonymous()
		{
		}
