##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=unitestmod
ConfigurationName      :=Release
WorkspacePath          := "/home/anandrathi/sre/unittest"
ProjectPath            := "/home/anandrathi/sre/unittest/unitestmod"
IntermediateDirectory  :=Release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Anand Rathi
Date                   :=03/08/2013
CodeLitePath           :="/home/anandrathi/.codelite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="unitestmod.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -O2
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)$(UNIT_TEST_PP_SRC_DIR)/src $(IncludeSwitch)/home/anandrathi/sre/libjspp/inc $(IncludeSwitch)/home/anandrathi/sre/js-1.8.5/js/src/build-debug/dist/include $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/core $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/ $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/ $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/postgresql $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/oracle $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/odbc $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/mysql $(IncludeSwitch)/home/anandrathi/sre/libcdr/libjspp_util/soci-3.1.0/backends/sqlite3 $(IncludeSwitch)/usr/include/oracle/11.2/client64/ $(IncludeSwitch)/usr/include/mysql/ $(IncludeSwitch)/usr/include/ $(IncludeSwitch)/home/anandrathi/sre/boost 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)nsl $(LibrarySwitch)dl $(LibrarySwitch)m $(LibrarySwitch)crypt $(LibrarySwitch)util $(LibrarySwitch)pthread $(LibrarySwitch)c $(LibrarySwitch)boost_regex $(LibrarySwitch)boost_unit_test_framework $(LibrarySwitch)anandjs185 $(LibrarySwitch)jspp 
ArLibs                 :=  "nsl" "dl" "m" "crypt" "util" "pthread" "c" "boost_regex" "boost_unit_test_framework" "anandjs185" "jspp" 
LibPath                := $(LibraryPathSwitch)/home/anandrathi/sre/js-1.8.5/js/src/build-debug/ $(LibraryPathSwitch). $(LibraryPathSwitch)$(UNIT_TEST_PP_SRC_DIR)/Release $(LibraryPathSwitch)/home/anandrathi/sre/js-1.8.5/js/src/build-debug/ $(LibraryPathSwitch)/home/anandrathi/sre/libcdr/libjspp/Debug $(LibraryPathSwitch)/home/anandrathi/sre/libcdr/libjspp_util/Debug $(LibraryPathSwitch)/usr/lib/oracle/11.2/client64/lib/ $(LibraryPathSwitch)/usr/lib64/mysql/ $(LibraryPathSwitch)/home/anandrathi/sre/libcdr/cppdb/Debug $(LibraryPathSwitch)/home/anandrathi/sre/boost_1_51_0/stage/lib $(LibraryPathSwitch)/home/anandrathi/sre/Debug/libjspp $(LibraryPathSwitch)/home/anandrathi/sre/instantclient_11_2/ $(LibraryPathSwitch)/home/anandrathi/sre/Debug/libjspp_util/ $(LibraryPathSwitch)/home/anandrathi/sre/Debug/cppdb/ $(LibraryPathSwitch)/home/anandrathi/sre/js-1.8.5/js/src/build-debug/dist/lib/ 

##
## Common variables
## AR, CXX, CC, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -Wall -fPIC --std=c++0x -ggdb3  -DDEBUG  -Wshadow -Wnon-virtual-dtor -Wsign-promo -Wextra -Winvalid-pch  -D_REENTRANT -D_GNU_SOURCE -fno-strict-aliasing -pipe -fstack-protector  -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64   -g -pipe  -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m64 -mtune=generic -D_SQ64 $(Preprocessors)
CFLAGS   :=   $(Preprocessors)


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
LD_LIBRARY_PATH:=/home/anandrathi/sre:/home/anandrathi/sre/js-1.8.5/js/src/build-debug/:/home/anandrathi/sre/libcdr/libjspp/Debug:/home/anandrathi/mygcc/lib64/:/home/anandrathi/sre/js-1.8.5/js/src//build-debug/:/home/anandrathi/sre/libcdr/libjspp/Debug:/home/anandrathi/sre/libcdr/libjspp_util/Debug:/usr/lib/oracle/11.2/client64/lib/:/usr/lib64/mysql/:/home/anandrathi/sre/boost_1_51_0/stage/lib:/home/anandrathi/sre/Debug/libjspp:/home/anandrathi/sre/instantclient_11_2/:/home/anandrathi/sre/Debug/libjspp_util/:/home/anandrathi/sre/Debug/cppdb/:
Objects0=$(IntermediateDirectory)/main$(ObjectSuffix) $(IntermediateDirectory)/utf8$(ObjectSuffix) $(IntermediateDirectory)/test_pack$(ObjectSuffix) $(IntermediateDirectory)/test_anonymous$(ObjectSuffix) $(IntermediateDirectory)/exportclasstest$(ObjectSuffix) $(IntermediateDirectory)/testpack_export$(ObjectSuffix) $(IntermediateDirectory)/inc_jsfunctionCallHandle$(ObjectSuffix) 

Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d Release || $(MakeDirCommand) Release

PreBuild:
	@echo Executing Pre Build commands ...
	export LIBRARY_PATH=/home/anandrathi/sre/js-1.8.5/js/src/build-debug/
	@echo Done


##
## Objects
##
$(IntermediateDirectory)/main$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main$(ObjectSuffix) -MF$(IntermediateDirectory)/main$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/utf8$(ObjectSuffix): utf8.cpp $(IntermediateDirectory)/utf8$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/utf8.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/utf8$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/utf8$(DependSuffix): utf8.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/utf8$(ObjectSuffix) -MF$(IntermediateDirectory)/utf8$(DependSuffix) -MM "utf8.cpp"

$(IntermediateDirectory)/utf8$(PreprocessSuffix): utf8.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/utf8$(PreprocessSuffix) "utf8.cpp"

$(IntermediateDirectory)/test_pack$(ObjectSuffix): test_pack.cpp $(IntermediateDirectory)/test_pack$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/test_pack.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/test_pack$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/test_pack$(DependSuffix): test_pack.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/test_pack$(ObjectSuffix) -MF$(IntermediateDirectory)/test_pack$(DependSuffix) -MM "test_pack.cpp"

$(IntermediateDirectory)/test_pack$(PreprocessSuffix): test_pack.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/test_pack$(PreprocessSuffix) "test_pack.cpp"

$(IntermediateDirectory)/test_anonymous$(ObjectSuffix): test_anonymous.cpp $(IntermediateDirectory)/test_anonymous$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/test_anonymous.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/test_anonymous$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/test_anonymous$(DependSuffix): test_anonymous.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/test_anonymous$(ObjectSuffix) -MF$(IntermediateDirectory)/test_anonymous$(DependSuffix) -MM "test_anonymous.cpp"

$(IntermediateDirectory)/test_anonymous$(PreprocessSuffix): test_anonymous.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/test_anonymous$(PreprocessSuffix) "test_anonymous.cpp"

$(IntermediateDirectory)/exportclasstest$(ObjectSuffix): exportclasstest.cpp $(IntermediateDirectory)/exportclasstest$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/exportclasstest.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/exportclasstest$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/exportclasstest$(DependSuffix): exportclasstest.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/exportclasstest$(ObjectSuffix) -MF$(IntermediateDirectory)/exportclasstest$(DependSuffix) -MM "exportclasstest.cpp"

$(IntermediateDirectory)/exportclasstest$(PreprocessSuffix): exportclasstest.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/exportclasstest$(PreprocessSuffix) "exportclasstest.cpp"

$(IntermediateDirectory)/testpack_export$(ObjectSuffix): testpack_export.cpp $(IntermediateDirectory)/testpack_export$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/unittest/unitestmod/testpack_export.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/testpack_export$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/testpack_export$(DependSuffix): testpack_export.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/testpack_export$(ObjectSuffix) -MF$(IntermediateDirectory)/testpack_export$(DependSuffix) -MM "testpack_export.cpp"

$(IntermediateDirectory)/testpack_export$(PreprocessSuffix): testpack_export.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/testpack_export$(PreprocessSuffix) "testpack_export.cpp"

$(IntermediateDirectory)/inc_jsfunctionCallHandle$(ObjectSuffix): ../../libjspp/inc/jsfunctionCallHandle.cpp $(IntermediateDirectory)/inc_jsfunctionCallHandle$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/anandrathi/sre/libjspp/inc/jsfunctionCallHandle.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/inc_jsfunctionCallHandle$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/inc_jsfunctionCallHandle$(DependSuffix): ../../libjspp/inc/jsfunctionCallHandle.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/inc_jsfunctionCallHandle$(ObjectSuffix) -MF$(IntermediateDirectory)/inc_jsfunctionCallHandle$(DependSuffix) -MM "../../libjspp/inc/jsfunctionCallHandle.cpp"

$(IntermediateDirectory)/inc_jsfunctionCallHandle$(PreprocessSuffix): ../../libjspp/inc/jsfunctionCallHandle.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/inc_jsfunctionCallHandle$(PreprocessSuffix) "../../libjspp/inc/jsfunctionCallHandle.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/main$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/main$(DependSuffix)
	$(RM) $(IntermediateDirectory)/main$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/utf8$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/utf8$(DependSuffix)
	$(RM) $(IntermediateDirectory)/utf8$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/test_pack$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/test_pack$(DependSuffix)
	$(RM) $(IntermediateDirectory)/test_pack$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/test_anonymous$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/test_anonymous$(DependSuffix)
	$(RM) $(IntermediateDirectory)/test_anonymous$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/exportclasstest$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/exportclasstest$(DependSuffix)
	$(RM) $(IntermediateDirectory)/exportclasstest$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/testpack_export$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/testpack_export$(DependSuffix)
	$(RM) $(IntermediateDirectory)/testpack_export$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/inc_jsfunctionCallHandle$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/inc_jsfunctionCallHandle$(DependSuffix)
	$(RM) $(IntermediateDirectory)/inc_jsfunctionCallHandle$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) "../.build-release/unitestmod"


