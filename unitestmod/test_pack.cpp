#include "test_anonymous.h"
#include <boost/test/unit_test.hpp>

using boost::unit_test_framework::test_suite;
using boost::unit_test_framework::test_case;
class AnonymousTestPack:public test_suite
{
public:

	 
AnonymousTestPack():test_suite("Anonymous") 
   {
	
	// creating instace of the class to be tested
     boost::shared_ptr<TestAnonymous> instance(new TestAnonymous());

      //creating test cases by passing the instance 
      test_case* testGenerateScript = BOOST_CLASS_TEST_CASE(&TestAnonymous::GenerateScript, instance );
      test_case* propertyVal = BOOST_CLASS_TEST_CASE(&TestAnonymous::PropertyValTester, instance );
      //adding test cases in test suite
	  add(testGenerateScript);
	  add(propertyVal);

   }
   
};
